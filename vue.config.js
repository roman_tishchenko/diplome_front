module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],

  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: []
    }
  }
}
